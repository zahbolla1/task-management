# Task Management

this project has these main following features

- user management
    * sign up
    * login
    * delete(authorization)
    * update (authorization)
- tasks management
    * get all tasks (open)
    * get single task (open)
    * comment a task (open)
    * upadate a task (authorization)
    * share task privilege ( update & share)


## Technologies used

- NodeJS (express) and TypeScript
- GraphQL with Apollo Server and Type GraphQL
- MongoDB Database integrated with Mongoose/TypeGoose
- Jest for testing (unit test, integration teset)
- Docker

## Available Scripts

In the project directory, you can run:

### `yarn start:dev`

Runs the app in the development mode.\
Open [http://localhost:4000](http://localhost:4000) to view the playground.

you can excute and test all features on the playground

### `yarn test`

Launches the test runner in the interactive watch mode.\

two tpes of tests were implemented
1. unit tests
2. integration test

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

### `yarn start`

❗ run this after yarn build

runs the server on production mode


## Env variables

* PORT: exposition port, default=4000
* NODE_ENV: running enviroment, default=development
* GRAPHQL_PATH: the entry point of apollo server: default=/graphql
* MONGO_DB: connection string of mongo database

you can ser all this params in .env in the project root
A skelet is provided in env-sample

## Docker
- Make sure you have docker installed on your machine
- Run `docker-compose up` to start the containers
- Run `docker-compose down` to remove the running containers

## Architecture
The project have been set over a ***DDD architecture*** .


##### Overview

```
.
├── src                        # Where your source code lives
│   ├── bootstrap              # Bootstrapping and loading of the API dependencies (Express, Apollo, Database, ...)
│   ├── application            # presentation layer: holders the dtos, error handling, and grapql queries and resolvers
│   ├── domain                 # logic layer: holds the main entites and services
│   ├── infrastucture          # persistance layer: repository definition , model definition, ORM
│   ├── tests                  # Where all our testing strategy lives
│   ├── utils                  # Collection of utils function that we use in the project
│   ├── configs                # Config of the app, sourced by environment variables, constants
│   └── index.ts               # Entry point of the API
│
├── jest-mongodb-config.js     # mock db configs
├── jest.config.js             # Jest configuration
├── docker-compose.yml         # Docker compose configuration
├── DockerFile                 # Docker file
├── .dockerignore              # Standard dockerignore file
├── .env-sample                # Example of what your .env file should look like
├── .gitignore                 # Standard gitignore file
├── package.json               # Node module dependencies
├── README.md                  # Simple readme file
└── tsconfig.json              # TypeScript compiler options
```


##### application

```
.
├── src
│   └── application
│       ├── auth            # set graphql context for autentication
│       ├── dtos            # input/output interface definitions
│       ├── error           # errors definition
│       ├── resolvers       # main resolvers definition
│       └── schema.graphql  # graphql schema definition
```
