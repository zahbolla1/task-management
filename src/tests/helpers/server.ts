import express from "express";

const { HttpLink } = require("apollo-link-http");
const fetch = require("node-fetch");
const { execute, toPromise } = require("apollo-link");
const { ApolloServer } = require("apollo-server-express");

// module.exports.toPromise = toPromise;

import typeDefs from "../../application/schema.graphql";
import resolvers from "../../application/resolvers";
import appLoader from "../../bootstrap/loaders/apollo";

// import defaultContext from "../../application/auth";

/**
 * Integration testing utils
 */
export const constructTestServer = (context = {}) =>
  new ApolloServer({
    typeDefs,
    resolvers,
    context,
    playground: true
  });

/**
 * e2e Testing Utils
 */

export const startTestServer = async () => {
  // if using apollo-server-express...
  const app = express();
  const server = constructTestServer();
  server.applyMiddleware({ app, path: "/g" });
  const httpServer = await app.listen(4545);

  // @ts-ignore
  const linkBuilder = (headers: any) =>
    new HttpLink({
      uri: `http://localhost:4545`,
      fetch,
      headers
    });

  // @ts-ignore
  const executeOperation = ({ query, variables = {} }, headers) =>
    execute(linkBuilder(headers), { query, variables });

  return {
    // stop: () => httpServer.server.close(),
    graphql: executeOperation
  };
};
