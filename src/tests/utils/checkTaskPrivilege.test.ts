import checkTaskPrivilege from "../../utils/checkTaskPrivilege";
import { UNAUTHORIZED } from "../../application/errors";

describe("checkTaskPrivilege", () => {
  const task = {
    owner: 1,
    shared: [2, 3]
  };

  it("The owner should have privilege to his own task", () => {
    const user = 1;
    expect(checkTaskPrivilege(task, user)).toBeFalsy();
  });
  it("A User should have privilege to shared task", () => {
    const user = 2;
    expect(checkTaskPrivilege(task, user)).toBeFalsy();
  });
  it("Deny acces to unautorised users", () => {
    const user = 10;
    expect(() => checkTaskPrivilege(task, user)).toThrow(UNAUTHORIZED);
  });
});
