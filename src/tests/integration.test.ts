import "graphql-import-node";
import { gql } from "apollo-server-express";
import { createTestClient } from "apollo-server-testing";
import {
  connect,
  closeDatabase,
  populateDatabase,
  clearDatabase
} from "./helpers/db";
import { constructTestServer } from "./helpers/server";
import taskModel from "../infrastructure/repository/models/tasks";

beforeAll(async () => {
  await connect();
  // fill data
  await populateDatabase(taskModel, [
    {
      id: 1,
      title: "test task",
      description: "description test",
      owner: 1,
      comments: [],
      shared: [],
      status: "open"
    },
    {
      id: 2,
      title: "test task",
      description: "description test",
      owner: 1,
      comments: [],
      shared: [],
      status: "open"
    },
    {
      id: 3,
      title: "test task",
      description: "description test",
      owner: 1,
      comments: [],
      shared: [2],
      status: "open"
    }
  ]);
});

afterAll(async (done) => {
  await clearDatabase();
  await closeDatabase();
  done();
});

describe("Tasks", () => {
  it("should get a task", async () => {
    // start server
    const server = await constructTestServer();

    const { query } = createTestClient(server);

    const GET_TASK = gql`
      query GetTask {
        task(id: 1) {
          id
          title
        }
      }
    `;

    // run query against the server and snapshot the output
    const res = await query({
      query: GET_TASK
    });

    expect(res).toMatchSnapshot();
  });

  describe("add Task", () => {
    const CREATE_TASK = gql`
      mutation {
        createTask(
          task: {
            title: " task number 1"
            description: "hello task"
            status: "open"
          }
        ) {
          id
          owner
          shared
          title
          description
          status
          comments {
            user
            date
            text
          }
        }
      }
    `;

    it("should be able to create a task when logged in", async () => {
      const server = await constructTestServer({ user: 1 });
      const { mutate } = createTestClient(server);

      const res = await mutate({
        mutation: CREATE_TASK
      });
      expect(res).toMatchSnapshot();
    });

    it("should not be able to create a task until log", async () => {
      const server = await constructTestServer();
      const { mutate } = createTestClient(server);
      const res = await mutate({
        mutation: CREATE_TASK
      });
      expect(res).toMatchSnapshot();
    });
  });

  describe("delete Task", () => {
    const DELETE_TASK = gql`
      mutation DeleteTask($id: Int!) {
        deleteTask(id: $id)
      }
    `;

    it("should be able to remove his own task", async () => {
      const server = await constructTestServer({ user: 1 });
      const { mutate } = createTestClient(server);
      const variables = {
        id: 1
      };
      const res = await mutate({
        mutation: DELETE_TASK,
        variables
      });
      expect(res).toMatchSnapshot();
    });

    it("should not be able to create a task until log", async () => {
      const server = await constructTestServer();
      const { mutate } = createTestClient(server);
      const variables = {
        id: 1
      };
      const res = await mutate({
        mutation: DELETE_TASK,
        variables
      });
      expect(res).toMatchSnapshot();
    });

    it("should be able to remove shared task", async () => {
      const server = await constructTestServer({ user: 2 });
      const { mutate } = createTestClient(server);
      const variables = {
        id: 3
      };
      const res = await mutate({
        mutation: DELETE_TASK,
        variables
      });
      expect(res).toMatchSnapshot();
    });
  });
});
