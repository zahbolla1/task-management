import { ITasksFilter } from "./ITasksFilter";
import { IBuiltTasksFilter } from "./IBuiltTasksFilter";

export interface ISubFilter {
  owner?: number;
  shared?: any;
  id?: any;
}

const tasksFilterBuilder = (filter: ITasksFilter = {}): IBuiltTasksFilter => {
  const subFilter: ISubFilter = {};
  if (filter.owner) subFilter.owner = filter.owner;
  if (filter.shared) subFilter.shared = { $in: filter.shared };
  if (filter.ids) subFilter.id = { $in: filter.ids };

  return {
    skip: filter.skip || 0,
    limit: filter.limit || 12,
    subFilter
  };
};

export default tasksFilterBuilder;
