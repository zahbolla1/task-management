export interface ITasksFilter {
  ids?: number[];
  owner?: number;
  shared?: number[];
  skip?: number;
  limit?: number;
}
