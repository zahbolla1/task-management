import { ISubFilter } from "./tasksFilterBuilder";

export interface IBuiltTasksFilter {
  skip: number;
  limit: number;
  subFilter: ISubFilter;
}
