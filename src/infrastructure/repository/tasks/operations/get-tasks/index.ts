import { ITask } from "../../../models/tasks/ITask";
import { TaskModel } from "../../../models";
import { ITasksFilter } from "./filter/ITasksFilter";
import tasksFilterBuilder from "./filter/tasksFilterBuilder";

const getTasks = async (filter: ITasksFilter = {}): Promise<ITask[]> => {
  const _filter = tasksFilterBuilder(filter);
  return TaskModel.find(_filter.subFilter)
    .skip(_filter.skip)
    .limit(_filter.limit)
    .lean();
};

export default getTasks;
