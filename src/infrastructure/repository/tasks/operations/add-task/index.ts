import { ITaskInput } from "./ITaskInput";
import countService from "../../../counts/counts.repository";
import { TaskModel } from "../../../models";
import { ITask } from "../../../models/tasks/ITask";
import getTask from "../get-task";

const addTask = async (task: ITaskInput): Promise<ITask> => {
  const counts = await countService.getCounts();
  const id = counts.task;
  const newTask = new TaskModel({
    id,
    ...task
  });
  await newTask.save();
  await countService.updateCounts("task");
  return getTask(id);
};

export default addTask;
