import { IComment } from "../../../models/tasks/comment/IComment";

export interface ITaskInput {
  owner: number;
  shared?: number[]; // default empty
  title: string;
  description: string;
  status?: string; // default is open
  comments?: IComment[]; // default empty
}
