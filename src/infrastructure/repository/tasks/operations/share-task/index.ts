import { ITask } from "../../../models/tasks/ITask";
import { TaskModel } from "../../../models";

const shareTask = async (id: number, to: number): Promise<ITask | null> =>
  TaskModel.findOneAndUpdate(
    { id },
    { $addToSet: { shared: to } },
    {
      new: true
    }
  );

export default shareTask;
