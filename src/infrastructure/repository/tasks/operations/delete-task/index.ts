import { TaskModel } from "../../../models";

const deleteTask = async (id: number): Promise<boolean> => {
  await TaskModel.deleteOne({ id });
  return true;
};

export default deleteTask;
