import { ITask } from "../../../models/tasks/ITask";
import { TaskModel } from "../../../models";

const getTask = async (id: number): Promise<ITask> => {
  return TaskModel.findOne({ id }, "-v").lean();
};

export default getTask;
