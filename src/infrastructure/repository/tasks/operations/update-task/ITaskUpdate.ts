export interface ITaskUpdate {
  owner?: number;
  title?: string;
  description?: string;
  status?: string;
}
