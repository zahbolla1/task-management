import { ITask } from "../../../models/tasks/ITask";
import { ITaskUpdate } from "./ITaskUpdate";
import { TaskModel } from "../../../models";

const updateTask = async (
  id: number,
  task: ITaskUpdate
): Promise<ITask | null> => {
  return TaskModel.findOneAndUpdate({ id }, task, {
    new: true
  });
};

export default updateTask;
