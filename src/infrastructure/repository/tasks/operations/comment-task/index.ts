import { ITask } from "../../../models/tasks/ITask";
import { TaskModel } from "../../../models";
import { IComment } from "../../../models/tasks/comment/IComment";

const commentTask = async (
  id: number,
  comment: IComment
): Promise<ITask | null> =>
  TaskModel.findOneAndUpdate(
    { id },
    { $push: { comments: comment } },
    {
      new: true
    }
  );

export default commentTask;
