import getTask from "./operations/get-task";
import getTasks from "./operations/get-tasks";
import addTask from "./operations/add-task";
import deleteTask from "./operations/delete-task";
import updateTask from "./operations/update-task";
import commentTask from "./operations/comment-task";
import shareTask from "./operations/share-task";

const tasksRepository = {
  getTask,
  getTasks,
  addTask,
  deleteTask,
  updateTask,
  commentTask,
  shareTask
};

export default tasksRepository;
