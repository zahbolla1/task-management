import getCounts from "./operations/getCounts";
import updateCounts from "./operations/updateCounts";

const countsRepository = {
  getCounts,
  updateCounts
};

export default countsRepository;
