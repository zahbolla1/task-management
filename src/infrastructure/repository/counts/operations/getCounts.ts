import { CountModel } from "../../models";
import { ICount } from "../../models/counts/ICount";

const getCounts = async (): Promise<ICount> => {
  // const c = CountRepository.findOne().lean();
  return CountModel.findOneAndUpdate(
    {},
    {},
    { setDefaultsOnInsert: true, upsert: true, new: true }
  ).lean();
};

export default getCounts;
