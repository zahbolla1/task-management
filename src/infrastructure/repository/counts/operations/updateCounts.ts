import { CountModel } from "../../models";
import { ICount } from "../../models/counts/ICount";
import getCounts from "./getCounts";

const updateCounts = async (item: string): Promise<ICount> => {
  const old = await getCounts();
  // @ts-ignore
  old[item] = old[item] + 1;
  return CountModel.findOneAndUpdate({}, old).lean();
};

export default updateCounts;
