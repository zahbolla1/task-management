const commentType = {
  user: { type: Number, required: true },
  text: {
    type: String,
    trim: true,
    required: true
  },
  date: { type: Number, default: Date.now() }
};

export default commentType;
