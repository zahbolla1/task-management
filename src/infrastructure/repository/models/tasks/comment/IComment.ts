export interface IComment {
  user: number;
  text: string;
  date?: number;
}
