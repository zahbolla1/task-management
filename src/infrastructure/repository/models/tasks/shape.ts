import taskStatus from "../../../../configs/constants";
import commentType from "./comment/commentType";

const taskShape = {
  id: {
    type: Number,
    unique: true
  },
  owner: {
    type: Number,
    required: true
  },
  shared: [Number],
  title: {
    type: String,
    trim: true
  },
  description: {
    type: String,
    trim: true
  },
  status: {
    type: String,
    enum: taskStatus,
    default: taskStatus[0]
  },
  comments: { type: [commentType], default: [] },
  createdAt: Number,
  updatedAt: Number
};

export default taskShape;
