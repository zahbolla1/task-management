import mongoose from "mongoose";
import { IComment } from "./comment/IComment";

export interface ITask {
  id: number;
  owner: number;
  shared: number[];
  title: string;
  description: string;
  status: string;
  comments: IComment[];
  createdAt: Number;
  updatedAt: Number;
}

export type TaskModel = mongoose.Document & {
  id: number;
  owner: number;
  shared: number[];
  title: string;
  description: string;
  status: string;
  comments: IComment[];
  createdAt: Number;
  updatedAt: Number;
};
