import { Schema, model } from "mongoose";
import taskShape from "./shape";
import { TaskModel } from "./ITask";

const Task = new Schema(taskShape, {
  collection: "tasks",
  timestamps: { currentTime: () => Date.now() },
  strict: true
});

export default model<TaskModel>("tasks", Task);
