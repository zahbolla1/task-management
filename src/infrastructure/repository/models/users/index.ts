import { Schema, model } from "mongoose";
import userShape from "./shape";
import { UserModel } from "./IUser";

const User = new Schema(userShape, {
  collection: "users",
  timestamps: { currentTime: () => Date.now() },
  strict: true
});

export default model<UserModel>("users", User);
