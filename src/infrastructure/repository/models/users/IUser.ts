import mongoose from "mongoose";

export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export type UserModel = mongoose.Document & {
  name: string;
  username: string;
  email: string;
  password: string;
  avatar: string;
  created_at: Date;
};
