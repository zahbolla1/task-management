const userShape = {
  id: {
    type: Number,
    unique: true
  },
  firstName: {
    type: String,
    maxlength: 48,
    trim: true
  },
  lastName: {
    type: String,
    maxlength: 48,
    trim: true
  },
  email: {
    type: String,
    toLowerCase: true,
    trim: true,
    maxlength: 100,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minLength: 6
  },
  createdAt: Number,
  updatedAt: Number
};

export default userShape;
