const countShape = {
  user: { type: Number, required: true, unique: true, default: 1 },
  task: { type: Number, required: true, unique: true, default: 1 }
};

export default countShape;
