import mongoose from "mongoose";

export interface ICount {
  user: number;
  task: number;
}

export type CountModel = mongoose.Document & {
  user: number;
  task: number;
};
