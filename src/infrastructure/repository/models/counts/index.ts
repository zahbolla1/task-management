import { Schema, model } from "mongoose";
import countShape from "./shape";
import { CountModel } from "./ICount";

const Count = new Schema(countShape, {
  collection: "counts",
  timestamps: { currentTime: () => Date.now() },
  strict: true
});

export default model<CountModel>("counts", Count);
