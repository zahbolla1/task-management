import getUserById from "./operations/get-user_by_id";
import getUserByEmail from "./operations/get-user_by_email";
import addUser from "./operations/add-user";
import updateUser from "./operations/update-user";
import deleteUser from "./operations/delete-user";

const userRepositoryService = {
  getUserById,
  getUserByEmail,
  addUser,
  updateUser,
  deleteUser
};

export default userRepositoryService;
