import { IUserUpdate } from "./IUserUpdate";
import { UserModel } from "../../../models";
import { IUser } from "../../../models/users/IUser";

const updateUser = async (id: number, user: IUserUpdate): Promise<IUser> =>
  UserModel.findOneAndUpdate(
    {
      id
    },
    user,
    { new: true }
  ).lean();

export default updateUser;
