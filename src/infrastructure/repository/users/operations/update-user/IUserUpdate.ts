export interface IUserUpdate {
  firstName: string;
  lastName: string;
  email: string;
}
