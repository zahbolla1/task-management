import { UserModel } from "../../../models";
import { IUser } from "../../../models/users/IUser";

const getUserById = async (id: number): Promise<IUser> => {
  return UserModel.findOne({ id }, "-password -v").lean();
};

export default getUserById;
