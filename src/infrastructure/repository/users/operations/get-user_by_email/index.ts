import { UserModel } from "../../../models";
import { IUser } from "../../../models/users/IUser";

const getUserByEmail = async (email: string): Promise<IUser> => {
  return UserModel.findOne({ email }).lean();
};

export default getUserByEmail;
