import { UserModel } from "../../../models";
import { IUserInput } from "./IUserInput";
import { IUser } from "../../../models/users/IUser";
import countService from "../../../counts/counts.repository";
import getUser from "../get-user_by_id";

const addUser = async (user: IUserInput): Promise<IUser> => {
  const counts = await countService.getCounts();
  const id = counts.user;
  const newUser = new UserModel({
    id,
    ...user
  });
  await newUser.save();
  await countService.updateCounts("user");
  return getUser(id);
};

export default addUser;
