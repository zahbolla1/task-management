import { UserModel } from "../../../models";

const deleteUser = async (id: number): Promise<boolean> => {
  await UserModel.deleteOne({ id });
  return true;
};

export default deleteUser;
