import { verify } from "jsonwebtoken";
import configs from "../configs";
import { IToSign } from "./generateToken";

const { secret } = configs;

const verifyToken = (token: string): IToSign | null => {
  try {
    const payload = verify(token, secret);
    // @ts-ignore
    return { user: payload.user };
  } catch (err) {
    return null;
  }
};

export default verifyToken;
