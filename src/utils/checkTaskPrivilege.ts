import { UNAUTHORIZED } from "../application/errors";

const checkTaskPrivilege = (task: any, id: number) => {
  if (task.owner === id) return;
  if (Array.isArray(task.shared) && task.shared.includes(id)) return;
  throw UNAUTHORIZED;
};

export default checkTaskPrivilege;
