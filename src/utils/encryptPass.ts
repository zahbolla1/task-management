import { hash } from "bcrypt";
const saltRounds = 10;

const encryptPass = async (password: string): Promise<string> =>
  await hash(password, saltRounds);

export default encryptPass;
