import { compare } from "bcrypt";

const validatePass = async (password: string, hash: string): Promise<boolean> =>
  await compare(password, hash);

export default validatePass;
