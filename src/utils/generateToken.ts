import { sign } from "jsonwebtoken";
import configs from "../configs";

const { secret } = configs;

export interface IToSign {
  user: number;
}

const generateToken = (obj: IToSign) => sign(obj, secret);

export default generateToken;
