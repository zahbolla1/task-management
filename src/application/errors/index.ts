const NOT_FOUND = new Error("Not Found");
const INVALID_CREDENTIAL = new Error("Invalid Credential");
const UNAUTHORIZED = new Error("Unauthorized");

export { NOT_FOUND, INVALID_CREDENTIAL, UNAUTHORIZED };
