import tasksResolvers from "./tasks.resolvers";
import usersResolvers from "./users.resolvers";

const resolvers = [tasksResolvers, usersResolvers];

export default resolvers;
