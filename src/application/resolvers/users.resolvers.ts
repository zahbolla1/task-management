import { usersService } from "../../domain/services";
import { ISignUpInputDto } from "../dtos/sign-up/ISignUpInputDto";
import { ILoginInput } from "../dtos/login/ILoginInput";

const usersResolvers = {
  Mutation: {
    signUp: async (_: any, args: { user: ISignUpInputDto }) => {
      const { user } = args;
      return usersService.signUp(user);
    },
    login: async (_: any, args: ILoginInput) => {
      const { email, password } = args;
      return usersService.login(email, password);
    },
    deleteUser: async (_: any, args: { id: number }) => {
      const { id } = args;
      return usersService.deleteUser(id);
    },
    updateUser: async (_: any, args: { id: number; user: ISignUpInputDto }) => {
      const { user, id } = args;
      return usersService.updateUser(id, user);
    }
  }
};

export default usersResolvers;
