import { tasksService } from "../../domain/services";
import { ITasksFilterDto } from "../dtos/tasks/ITasksFilterDto";
import { ITaskInputDto } from "../dtos/tasks/ITaskInputDto";

const tasksResolvers = {
  Query: {
    task: async (_: any, args: { id: number }) => {
      const { id } = args;
      // dto
      return tasksService.getTask(id);
    },
    tasks: async (_: any, args: { filter: ITasksFilterDto }) => {
      const { filter } = args;
      return tasksService.getTasks(filter);
    }
  },
  Mutation: {
    createTask: async (_: any, args: { task: ITaskInputDto }, ctx: any) => {
      const { task } = args;
      const { user } = ctx;
      return tasksService.addTask(task, user);
    },
    commentTask: async (
      _: any,
      args: { id: number; text: string },
      ctx: any
    ) => {
      const { id, text } = args;
      const { user } = ctx;
      return tasksService.commentTask(id, user, text);
    },
    shareTask: async (_: any, args: { id: number; to: number }, ctx: any) => {
      const { id, to } = args;
      const { user } = ctx;
      return tasksService.shareTask(id, user, to);
    },
    deleteTask: async (_: any, args: { id: number }, ctx: any) => {
      const { id } = args;
      const { user } = ctx;
      return tasksService.deleteTask(id, user);
    },
    updateTask: async (
      _: any,
      args: { id: number; task: ITaskInputDto },
      ctx: any
    ) => {
      const { id, task } = args;
      const { user } = ctx;
      return tasksService.updateStatus(id, user, task);
    }
  }
};

export default tasksResolvers;
