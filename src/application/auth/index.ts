import { verifyToken } from "../../utils";

// @ts-ignore
const context = ({ req }) => {
  const auth = req.headers.authorization || "";
  if (!auth) return;
  const token = auth.replace("Bearer ", "");
  return verifyToken(token);
};

export default context;
