export interface ISignUpUpdateDto {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
}
