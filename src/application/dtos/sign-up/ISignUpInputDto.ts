export interface ISignUpInputDto {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}
