export interface ISignUpDisplay {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
}
