export interface ITasksFilterDto {
  ids?: number[];
  skip?: number;
  limit?: number;
  owner?: number;
  shared?: number[];
}
