import { IComment } from "../../../infrastructure/repository/models/tasks/comment/IComment";

export interface ITaskDisplayDto {
  id: number;
  owner: number;
  shared: number[];
  title: string;
  description: string;
  status: string;
  comments: IComment[];
  createdAt: Number;
  updatedAt: Number;
}
