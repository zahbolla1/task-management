export interface ITaskUpdateDto {
  owner?: number;
  title?: string;
  description?: string;
  status?: string;
}
