import { IComment } from "../../../infrastructure/repository/models/tasks/comment/IComment";

export interface ITaskInputDto {
  shared?: number[];
  title: string;
  description: string;
  status?: string;
  comments?: IComment[];
}
