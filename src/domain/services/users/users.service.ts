import signUp from "./helpers/signUp";
import login from "./helpers/login";
import { usersRepository } from "../../../infrastructure/repository/index.repository";

const userService = {
  signUp,
  login,
  deleteUser: usersRepository.deleteUser,
  updateUser: usersRepository.updateUser
};

export default userService;
