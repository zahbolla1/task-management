import { usersRepository } from "../../../../infrastructure/repository/index.repository";
import { INVALID_CREDENTIAL } from "../../../../application/errors";
import { generateToken, validatePass } from "../../../../utils";

const login = async (email: string, password: string): Promise<string> => {
  const user = await usersRepository.getUserByEmail(email);
  if (!user) throw INVALID_CREDENTIAL;
  try {
    await validatePass(password, user.password);
  } catch (e) {
    throw INVALID_CREDENTIAL;
  }

  return generateToken({ user: user.id });
};

export default login;
