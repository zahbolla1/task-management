import { usersRepository } from "../../../../../infrastructure/repository/index.repository";
import reassignRelatedTasks from "./reassignRelatedTasks";

const deleteUser = async (id: number): Promise<boolean> => {
  await usersRepository.deleteUser(id);
  reassignRelatedTasks(id).then().catch();
  return true;
};

export default deleteUser;
