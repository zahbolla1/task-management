import { ISignUpInputDto } from "../../../../application/dtos/sign-up/ISignUpInputDto";
import { ISignUpDisplay } from "../../../../application/dtos/sign-up/ISignUpDisplay";
import { usersRepository } from "../../../../infrastructure/repository/index.repository";
import { encryptPass } from "../../../../utils";

const signUp = async (user: ISignUpInputDto): Promise<ISignUpDisplay> => {
  user.password = await encryptPass(user.password);
  return usersRepository.addUser(user);
};

export default signUp;
