import getTask from "./helpers/getTask";
import getTasks from "./helpers/getTasks";
import addTask from "./helpers/addTask";
import commentTask from "./helpers/commentTask";
import deleteTask from "./helpers/deleteTask";
import updateStatus from "./helpers/updateStatus";
import shareTask from "./helpers/shareTask";

const taskService = {
  getTask,
  getTasks,
  addTask,
  shareTask,
  deleteTask,
  commentTask,
  updateStatus
};

export default taskService;
