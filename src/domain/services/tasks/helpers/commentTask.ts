import { ITaskDisplayDto } from "../../../../application/dtos/tasks/ITaskDisplayDto";
import { tasksRepository } from "../../../../infrastructure/repository/index.repository";

const commentTask = async (
  id: number,
  user: number,
  text: string
): Promise<ITaskDisplayDto | null> =>
  tasksRepository.commentTask(id, {
    user,
    text
  });

export default commentTask;
