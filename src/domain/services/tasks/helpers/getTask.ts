import { tasksRepository } from "../../../../infrastructure/repository/index.repository";
import { ITaskDisplayDto } from "../../../../application/dtos/tasks/ITaskDisplayDto";

const getTask = async (id: number): Promise<ITaskDisplayDto | null> =>
  tasksRepository.getTask(id);

export default getTask;
