import { tasksRepository } from "../../../../infrastructure/repository/index.repository";
import { checkTaskPrivilege } from "../../../../utils";

const deleteTask = async (id: number, user: number): Promise<boolean> => {
  const task = await tasksRepository.getTask(id);
  checkTaskPrivilege(task, user);
  return tasksRepository.deleteTask(id);
};

export default deleteTask;
