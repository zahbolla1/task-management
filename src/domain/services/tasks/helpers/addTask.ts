import { ITaskInputDto } from "../../../../application/dtos/tasks/ITaskInputDto";
import { ITaskDisplayDto } from "../../../../application/dtos/tasks/ITaskDisplayDto";
import { tasksRepository } from "../../../../infrastructure/repository/index.repository";

const addTask = (task: ITaskInputDto, user: number): Promise<ITaskDisplayDto> =>
  tasksRepository.addTask({
    ...task,
    owner: user
  });

export default addTask;
