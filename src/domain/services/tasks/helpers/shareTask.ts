import { ITaskDisplayDto } from "../../../../application/dtos/tasks/ITaskDisplayDto";
import { tasksRepository } from "../../../../infrastructure/repository/index.repository";
import { NOT_FOUND } from "../../../../application/errors";
import { checkTaskPrivilege } from "../../../../utils";

const shareTask = async (
  id: number,
  user: number,
  to: number
): Promise<ITaskDisplayDto | null> => {
  const ts = await tasksRepository.getTask(id);
  if (!ts) throw NOT_FOUND;
  checkTaskPrivilege(ts, user);

  // check if given users exists
  return tasksRepository.shareTask(id, to);
};

export default shareTask;
