import { tasksRepository } from "../../../../infrastructure/repository/index.repository";
import { ITaskUpdateDto } from "../../../../application/dtos/tasks/ITaskUpdateDto";
import { ITaskDisplayDto } from "../../../../application/dtos/tasks/ITaskDisplayDto";
import { NOT_FOUND } from "../../../../application/errors";
import { checkTaskPrivilege } from "../../../../utils";

const updateStatus = async (
  id: number,
  user: number,
  task: ITaskUpdateDto
): Promise<ITaskDisplayDto> => {
  const ts = await tasksRepository.getTask(id);
  if (!ts) throw NOT_FOUND;
  checkTaskPrivilege(ts, user);
  // @ts-ignore
  return tasksRepository.updateTask(id, task);
};

export default updateStatus;
