import { tasksRepository } from "../../../../infrastructure/repository/index.repository";
import { ITaskDisplayDto } from "../../../../application/dtos/tasks/ITaskDisplayDto";
import { ITasksFilterDto } from "../../../../application/dtos/tasks/ITasksFilterDto";

const getTasks = async (filter: ITasksFilterDto): Promise<ITaskDisplayDto[]> =>
  tasksRepository.getTasks(filter);

export default getTasks;
