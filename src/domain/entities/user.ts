import { ObjectType, Field } from "type-graphql";
import { prop } from "@typegoose/typegoose";

@ObjectType()
export class User {
  @prop()
  @Field()
  public id!: number;

  @prop({ trim: true, maxlength: 48 })
  @Field()
  firstName!: string;

  @prop({ trim: true, maxlength: 48 })
  @Field()
  lastName!: string;

  @prop({ trim: true, lowercase: true })
  @Field()
  email!: string;

  @prop()
  @Field()
  password!: string;

  @prop()
  @Field()
  createdAt?: number;

  @prop()
  @Field()
  updatedAt?: number;
}
