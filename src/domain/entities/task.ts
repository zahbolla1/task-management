import { ObjectType, Field } from "type-graphql";
import { prop } from "@typegoose/typegoose";
import taskStatus from "../../configs/constants";

interface IComment {
  user: number;
  text: string;
  date?: number;
}

@ObjectType()
export class Task {
  @prop({ required: true, unique: true })
  @Field()
  public id!: number;

  @prop({ trim: true, required: true })
  @Field()
  public title!: string;

  @prop({ trim: true })
  @Field()
  public description!: string;

  @prop({ enum: taskStatus, default: taskStatus[0] })
  @Field()
  public status!: string;

  @prop({ required: true })
  @Field()
  public owner!: number;

  @prop()
  @Field()
  public shared?: number[];

  @prop()
  @Field()
  public comments?: IComment[];

  @prop()
  @Field()
  public createdAt: number;

  @prop()
  @Field()
  public updatedAt!: number;
}
