import "reflect-metadata";

import bootstrap from "./bootstrap";
import configs from "./configs";

bootstrap(configs)
  .then(() => {})
  .catch((e) => console.log(e));
