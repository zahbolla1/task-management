import envVars from "dotenv";
import { Config } from "./configs.type";

envVars.config();

// All your secrets, keys go here
const configs: Config = {
  port: Number(process.env.PORT) || 4000,
  nodeEnv: process.env.NODE_ENV || "development",
  graphqlPath: process.env.GRAPHQL_PATH || "/graphql",
  mongoDB: process.env.MONGO_DB || "",
  secret: process.env.secret || "mySecret"
};

export default configs;
