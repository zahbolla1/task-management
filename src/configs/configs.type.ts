export interface Config {
  port: number;
  nodeEnv: string;
  graphqlPath: string;
  mongoDB: string;
  secret: string;
}
