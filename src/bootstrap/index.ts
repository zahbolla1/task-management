import express from "express";
import mongoose from "mongoose";

import appLoader from "./loaders";
import configs from "../configs";
import { Config } from "../configs/configs.type";

const { graphqlPath } = configs;

export default async (config: Config) => {
  const app = express();

  const server = await appLoader(app);

  server.applyMiddleware({
    app,
    path: graphqlPath,
    // Health check on /.well-known/apollo/server-health
    onHealthCheck: async () => {
      if (mongoose.connection.readyState === 1) return;

      throw new Error();
    }
  });

  app.listen({ port: config.port }, () =>
    console.log(
      `🚀 Server ready at http://localhost:${config.port}${config.graphqlPath}`
    )
  );
};
