import "graphql-import-node";
import { ApolloServer } from "apollo-server-express";
import configs from "../../configs";
import * as typeDefs from "../../application/schema.graphql";
import resolvers from "../../application/resolvers";
import context from "../../application/auth";

const { nodeEnv } = configs;

export default async () => {
  return new ApolloServer({
    introspection: true,
    playground: nodeEnv === "development",
    typeDefs,
    resolvers,
    context
  });
};
